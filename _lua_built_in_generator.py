""" This module allows for automatically fetching Lua built-ins from the Lua documentation. """

import re
import requests
from time import sleep
from json import dump


def _get_lua_built_in_doc(version='5.1') -> dict[str, list[str]]:
    """ Creates a dict of Lua built-ins from the Lua documentation.

    :param version: The version of Lua to get the built-ins for.

    :return: A dict of Lua built-ins.
    """
    lua_doc_table_pattern_str: str = r'''<h3><a name="functions">Lua functions</a></h3>([\w\W]*?)<h3><a name="api">C API</a></h3>'''
    basic_pattern_str: str = r'''^([\w\W]*)(?:</p><h3><a name="metamethods">metamethods</a></h3>|<h3><A name="env">)'''
    metamethod_pattern_str: str = r'''<h3><a name="metamethods">metamethods</a></h3>([\w\W]*)<h3><a name="env">environment<br>variables</a></h3>'''
    environment_variables_pattern_str: str = r'''<h3><a name="env">environment<br>variables</a></h3>([\w\W]*)'''
    entry_pattern_str: str = r'''" ?>([\w\W]*?)</a'''

    lua_doc_url: str = f'https://www.lua.org/manual/{version}/contents.html'

    lua_doc: str = str(requests.get(lua_doc_url, timeout=60).content)
    lua_doc_table: str = re.findall(lua_doc_table_pattern_str, lua_doc, re.IGNORECASE)[0]
    basic: str = re.findall(basic_pattern_str, lua_doc_table, re.IGNORECASE | re.MULTILINE)[0]
    if version != '5.1' and version != '5.2':
        metamethods: str = re.findall(metamethod_pattern_str, lua_doc_table, re.IGNORECASE)[0]
    environment_variables: str = re.findall(environment_variables_pattern_str, lua_doc_table, re.IGNORECASE)[0]

    built_in_dict: dict[str, list[str]] = {'basic': re.findall(entry_pattern_str, basic, re.IGNORECASE)}
    if version != '5.1' and version != '5.2':
        # noinspection PyUnboundLocalVariable
        built_in_dict['metamethods'] = re.findall(entry_pattern_str, metamethods, re.IGNORECASE)
    else:
        built_in_dict['metamethods'] = []
    built_in_dict['environment_variables'] = re.findall(entry_pattern_str, environment_variables, re.IGNORECASE)

    # Remove 'basic' from list
    built_in_dict['basic'].remove('basic')

    return built_in_dict


def _persistent_retriever(version='5.1') -> dict[str, list[str]]:
    """ Creates a dict of Lua built-ins from the Lua documentation, retrying until it succeeds.

    :param version: The version of Lua to get the built-ins for.

    :return: A dict of Lua built-ins.
    """
    tries: int = 60
    while tries > 0:
        try:
            return _get_lua_built_in_doc(version)
        except Exception as e:
            tries -= 1
            print(f'Failed to retrieve Lua built-ins with error:\n{e}\n\n{tries} tries left. Retrying in 1 second.')
            sleep(1)
            if tries <= 0:
                raise Exception('Failed to retrieve Lua built-ins.') from e


def create_lua_built_in_cache() -> None:
    """ Creates an offline cache of Lua built-ins.
    """
    dict_51: dict[str, list[str]] = _persistent_retriever('5.1')
    dict_52: dict[str, list[str]] = _persistent_retriever('5.2')
    dict_53: dict[str, list[str]] = _persistent_retriever('5.3')
    dict_54: dict[str, list[str]] = _persistent_retriever('5.4')

    master_dict: dict[float, dict[str, list[str]]] = {
        '5.1': dict_51,
        '5.2': dict_52,
        '5.3': dict_53,
        '5.4': dict_54,
    }

    with open('lua_auto_doc/modules/highlighter/LuaBuiltInCache.json', 'w', encoding='utf-8') as file:
        dump(master_dict, file)


if __name__ == '__main__':
    create_lua_built_in_cache()
