--- @module SecondModule
-- This module has no description, meaning the default description will be used.


--- A Classic
--- @return string Hello World!
--- @deprecated This boring old function is deprecated.
local function test()
    print("Hello World!")
end


--- @param num number A number to test.
--- @return string An advanced twist to the classic.
--- @internal And this advanced function is still for internal use only.
local function more_advanced_test(num)
    print("Hello World times " .. num .. "!")
end


-- Since this function does not have a docstring, it will not be shown in the documentation.
local function lonely()
    print("I'm all alone :(")
end


--- @module Out of Nowhere
--- You can have multiple modules in the same file. The parser will change the associated module once it
--- encounters a new module declaration.
--[[ Please take note there must be an empty line between module declaration and the first docstring.
    Comments do not count as empty lines.
]]
--- Which is why this line will show up in the module description.
--- @internal
-- You can also mark an entire module as either @internal or @deprecated.
-- This will change the background colour of the object entries to mark them as such. There will also be added
-- a warning at the top of the module.

--- Filler function
local function filler()
    print("Filler")
end

--- @module SecondModule
-- Let's change back to SecondModule again.

--- This is a basic function.
function basic_test()
    print("Hello")
end
