--- @module Main Module
--- This the is the main module of the project. \n
--- Please ignore the actual code and just pay attention to the docstrings.
--- The code is just there to give some context to the docstrings.
--- This @see SecondModule [module: SecondModule] will hyperlink to the SecondModule module inside the module
--- description.


--- Multiplies a list of numbers by a ---@see https://en.wikipedia.org/wiki/Scalar [link_text: scalar].
--- @param param1 number The scalar to multiply the list of numbers by.
--- @param param2 table<number> A List of number to multiply.
--- @return table<number> A list of numbers multiplied by the scalar.
--- @return string? Returns the sum of the list as a string if the sum is greater than 1000.
local function multiply(param1, param2)
    local result = {}
    local sum = 0
    for i, v in ipairs(param2) do
        result[i] = v * param1
        sum = sum + result[i]
    end
    if sum > 1000 then
        return result, tostring(sum)
    end
    return result
end



--- @class MainClass
--- @field name string The name of the object.
--- @field age number The age of the object.
--- @property class_type MainClass The object type
--- This is just an example class.
MainClass = {
    name = nil,
    age = nil,
    class_type = "MainClass"
}

--- @param name string The name of the object.
--- @param age number The age of the object.
--- @return MainClass
function MainClass:new(name, age)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.age = age
    return o
end


--- @param scaler number Just a scaler
--- @return fun(third_number: number): number
function MainClass:closure(scaler)
    return function(third_number)
        return self.age * scaler * third_number
    end
end


--- This function returns a list of MainClass objects. Take notice of how the return type is presented
--- in the documentation.
--- @see MiniMainClass:generate() [link_text: MainClass' subclass] For a method that overrides the inherited method.
--- @return table<MainClass>
function MainClass:generate()
    local result = {}
    for i = 1, 10 do
        result[i] = self:new(self.name, self.age + i)
    end
    return result
end


-- We can also group methods together using the @group <id> tag

--- @group Getters
--- @return string
--- Get the name
function MainClass:get_name()
    return self.name
end

--- @group Getters
--- @return number
--- Get the age
function MainClass:get_age()
    return self.age
end


--- @group Setters
--- @param name string
--- Set the name
function MainClass:set_name(name)
    self.name = name
end

--- @group Setters
--- @param age number
--- Set the age
function MainClass:set_age(age)
    self.age = age
end
