-- Sometimes there are parts of the code that you want documented for your own use in your IDE,
-- but don't want to show in your documentation. For that you have two modules, @ignore and hidden <tags>.
-- Unlike @ignoreFile, these two tags will only ignore/hide documented objects, not the file itself.


-- @hidden got tags that allows you to "un-hide" objects via the config file.
-- For example setting the config option <"show_hidden": ["release_1.5"]> will include all
-- objects with the hidden tag release_1.5 in the documentation, regardless of any other tags they might have.
-- Tags are separated by space and any commas in the tags will be ignored.
-- Setting the config to <True> will reveal every hidden object.

--- @hidden release_1.5 dev_only
--- This function should stay hidden alongside until release 1.5
--- @return boolean
function is_release_1_5()
    return true
end

--- @hidden release_1.5 dev_only
--- @return nil
function release_1_5_function()
end


--- @hidden release_2.0 dev_only
--- @return nil
function release_2_0_function()
end


-- If you want to complete hide an object from the documentation you can use the @ignore tag.
-- Similar to @ignoreFile, when the parser encounters this tag, it will throw away the doc object.
-- Do note that if you have source code visibility enabled (the default), users can still find the object
-- in the source code linked in the documentation.
---@ignore
---@return number Returns 666
function evil_function()
end
