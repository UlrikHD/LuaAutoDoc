-- There is not a module declaration above documented objects, the objects will default to either
-- the class page or the functions page.


--- We can reference module with space as long as we wrap the name in <">.
--- For example ---@see "Main Module"
--- @property class_type MainClass
--- @property class_type2 MiniMainClass
--- @class dummy_class
dummy_class = {
    class_type = MainClass:new('name', 1),
    class_type2 = MiniMainClass:new('name', 1, 'extra_field')
}


--- @field dummy_1 dummy_class
--- @field dummy_2 dummy_class
--- @class dummy_class2
dummy_class2 = {
    dummy_1 = nil,
    dummy_2 = nil
}


--- @param dummy_1 dummy_class
--- @param dummy_2 dummy_class
--- @return dummy_class2
dummy_class2:new(dummy_1, dummy_2)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.dummy_1 = dummy_1
    o.dummy_2 = dummy_2
    return o


-- Since this method isn't documented it won't show up in the documentation.
function dummy_class2:dummy_method()
end


--- Everyone are odd in some way.
--- @return boolean
function dummy_class2:is_odd()
    return true
end
