--- @module Main Module
-- This is a separate file, however it will be shown together with the main.lua since they share the same module

--- @class MiniMainClass : MainClass
--- @superclass MainClass This is an optional tag, if you want to be more explicit than the format right above.
--- @field extra_field string This mini class is in fact not a mini class since it inherits from MainClass, meaning it's bigger!
MiniMainClass = MainClass:new()

--- @param name string The name of the object.
--- @param age number The age of the object.
--- @param extra_field string? An optional extra field for this mini class.
--- @author Mini me
--- This class is authored by a specific person, maybe not the author(s) configured in the config file?
--- @return MiniMainClass
function MiniMainClass:new(name, age, extra_field)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.name = name
    o.age = age
    if extra_field then
        o.extra_field = extra_field
    else
        o.extra_field = "Default value"
    end
    return o
end



--- This function returns a list of MainClass objects. Take notice of how the return type is presented
--- in the documentation.
--- @see MainClass:generate() [link_text: This method's supermethod] [rel_path: Script/main.lua] Just an example of @see
--- @return table<MiniMainClass>
function MiniMainClass:generate()
    local result = {}
    for i = 1, 10 do
        result[i] = self:new(self.name, self.age + i, self.extra_field)
    end
    return result
end
