-- There is not a module declaration above documented objects, the objects will default to either
-- the class page or the functions page.

--- An empty example function.
local function example()
end

--- Another empty example function.
function example2()
end

--- This function is grumpy, make sure to feed it a big number!
--- @param num number A number to test.
--- @return string The mood of the function.
--- @raise HungryError 2 If the number fed is less than 1, this function will throw an error moving two levels up.
function grumpy(num)
    if num < 1 then
        error('This is nothing!', 2)
    elseif num < 100 then
        return 'meh'
    elseif num < 1000 then
        return 'I still want more!'
    end
    return 'I am satisfied!'
end
